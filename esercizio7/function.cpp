#include "../esercizio6/list/linked_list.hpp"
#include <iostream>
#include "string"
#include <vector>
using namespace std;

void clear(Linked_list<int> &l)  {
   l.clear();
}

int num_elements(Linked_list<int>::position p, Linked_list<int>::position p1, Linked_list<int> &l){
    int num=0;
    while(p!=p1) {
        ++num;
        p=l.next(p);
    } 
    return num;
}

void move_min_max(Linked_list<int> &l) {
  
    int min = l.read(l.begin());
    int max = l.read(l.get_end());
    Linked_list<int>::position p1=l.begin(), p2=l.get_end();
    for(Linked_list<int>::position p =l.begin(); !(l.end(p)); p=l.next(p)) {
        if(min>l.read(p)) {
            min =l.read(p);
            p1=p;
        }

    }
    l.swap(p1,l.begin());

    for(Linked_list<int>::position p =l.get_end(); p!= l.previous(l.begin());  p=l.previous(p)) {
        if(max<l.read(p)) {
            max=l.read(p);
            p2=p;
        }
    }
  
  
  l.swap(p2,l.get_end());
  
}

void insertion_sort(Linked_list<int> &l) {
    int val;
    Linked_list<string>::position b;
     for(Linked_list<>::position p =l.next(l.begin()); !(l.end(p)); p=l.next(p)) {
        val=l.read(p);
        b= l.previous(p);
        while( l.next(b)>=l.begin() &&  l.read(b)> val) {
        
                l.write(l.read(b),l.next(b));
               
            b= l.previous(b);

        }
         l.write(val,l.next(b));
     }
}

void remove_equals(Linked_list<int> &l1, Linked_list<int> &l2) {
    int value;
     for(Linked_list<int>::position p =l2.begin(); !(l2.end(p)); p=l2.next(p)){
        value=l2.read(p);
            if(l1.find(value))
                l1.erase(l1.find(value)); 
     }

}
int main(){

    Linked_list<int> lista;
    lista.push_back(18);
    lista.push_back(1);
    lista.push_back(3);
    lista.push_back(16);
    lista.push_back(2);
    lista.push_back(7);

    Linked_list<int> lista2;
    lista2.push_back(1);
    lista2.push_back(2);
    lista2.push_back(2);
    lista2.push_back(1);

    insertion_sort(lista);
    cout<<lista<<endl;
    


    cout << lista2.palidrome() <<endl;

return 0;  
}