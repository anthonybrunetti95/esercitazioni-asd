#include "../../esercizio6/list/linked_list.hpp"
#include <iostream>
#include "string"
#include <vector>
using namespace std;


class polinomio
{
public:
	polinomio();
	~polinomio();
	int grado();
	void input();
	void output(std::ostream&)const;
	void somma(const polinomio &p);
	void moltiplica(const polinomio &b);
	int valore(int x);
	friend std::ostream& operator<<(std::ostream&, const polinomio&);
private:
	Linked_list<int> num;
	Linked_list<string> pol;
	Linked_list<int> esp;
	
};

polinomio::polinomio() {

}

polinomio::~polinomio() {

}

int polinomio::grado() {
	int max=0;
	for(auto p= esp.begin(); !esp.end(p); p=esp.next(p)) {
		if(max<esp.read(p)){
			max=esp.read(p);
		}
	}
	return max;
}

void polinomio::input() {
	int n;
	string a;
	int b;
	cin>>n;
	cin>>a;
	cin>>b;
	num.push_back(n);
	pol.push_back(a);
	esp.push_back(b);
	return ;
}

void polinomio::output(std::ostream& os) const {
	
	os<<num;
	os<<pol;
	os<<esp;


} 


void polinomio::somma(const polinomio &polinomio) {
	auto p= num.begin();
	auto b= pol.begin();
	auto q = esp.begin();
	auto p1 = polinomio.num.begin();
	auto b1 = polinomio.pol.begin();
	auto q1 = polinomio.esp.begin();


		
	while(!pol.end(b)) 
		{

			if(polinomio.pol.find(pol.read(b))) {
				if(polinomio.esp.find(esp.read(q1)))
				{
					this->num.write(num.read(p)+polinomio.num.read(p1), p);
				}
		}
		b= pol.next(b);
		p= num.next(p);
		p1=polinomio.num.next(p1);
	}
	p1=polinomio.num.begin();
	while(!polinomio.pol.end(b1)) {
			
				if(pol.find(polinomio.pol.read(b1))== nullptr || esp.find(polinomio.esp.read(q1))== nullptr) {
					num.push_back(polinomio.num.read(p1));
					pol.push_back(polinomio.pol.read(b1));
					esp.push_back(polinomio.esp.read(q1));
					p1=polinomio.num.next(p1);
					q1=polinomio.esp.next(q1);
				}
		
		b1=polinomio.pol.next(b1);
	
	}

}


void polinomio::moltiplica(const polinomio &polinomio) {
	auto p= num.begin();
	auto b= pol.begin();
	auto q = esp.begin();
	auto p1 = polinomio.num.begin();
	auto b1 = polinomio.pol.begin();
	auto q1 = polinomio.esp.begin();
	while(!pol.end(b)) 
		{

			if(polinomio.pol.find(pol.read(b))) 
			
				{
					this->num.write(num.read(p)*polinomio.num.read(p1), p);
					this-> esp.write(esp.read(q)+polinomio.esp.read(q1),q);
				}
		
		b= pol.next(b);
		p= num.next(p);
		p1=polinomio.num.next(p1);
		}
/*	while(!polinomio.pol.end(b1)) {
			
		if(pol.find(polinomio.pol.read(b1))== nullptr) {
				
				pol.push_back(polinomio.pol.read(b1));
				esp.push_back(polinomio.esp.read(q1));
				p1=polinomio.num.next(p1);
				q1=polinomio.esp.next(q1);
			}
		
	b1=polinomio.pol.next(b1);
	
}	
*/
}

int polinomio::valore(int x) {
	return 
}

ostream& operator<<(std::ostream& os, const polinomio &polinomio) {
	os<<"polinomio"<<endl;
	polinomio.output(os);
	os<<"========="<<endl;
	return os;

}