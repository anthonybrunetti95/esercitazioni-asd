#include "../esercizio6/list/linked_list.hpp"

#include "string"
#include <iostream>
using namespace std;


class eo_strings {
// inserisce inserisce una stringa nella corrispondente lista
public:
	eo_strings();
	void inserisci(string s);
	// rimuove la stringa dalla corrispondente lista
	void rimuovi(string s);
	// rimuove le stringhe di lunghezza pari che iniziano con il carattere c
	void rimuovi(char c);
	// restituisce il numero di stringhe di lunghezza pari
	int freq_pari();
	// visualizza le stringhe di lunghezza dispari ordinate per lunghezza
	void visualizza_dispari();
private:
	Linked_list<string> pari;
	Linked_list<string> dispari;
};


eo_strings::eo_strings() {

}

void eo_strings::inserisci(string s) {
	if(s.size()%2==0) {
		pari.push_back(s);
	}
	else
		dispari.push_back(s);
}

void eo_strings::rimuovi(string s) {
	if(pari.find(s)!=nullptr) {
		pari.erase(pari.find(s));
	}
	else if(dispari.find(s)!=nullptr) {
		dispari.erase(dispari.find(s));
	}
}

void eo_strings::rimuovi(char c) {
	for(auto p=pari.begin(); !pari.end(p); p=pari.next(p)){
		(pari.read(p))[0] == c;
			pari.erase(p);
			return ;
	}
		for(auto p=dispari.begin(); !dispari.end(p); p=dispari.next(p)){
		(dispari.read(p))[0] == c;
			dispari.erase(p);
			return ;
	}
}

void eo_strings::visualizza_dispari() {
	string val;
	Linked_list<string> l(dispari);
	Linked_list<string>::position b;
   	for(Linked_list<string>::position p =l.next(l.begin()); !(l.end(p)); p=l.next(p)) {
       val=l.read(p);
       b= l.previous(p);
    	while( l.next(b)>=l.begin() &&  l.read(b)> val) {
        		l.write(l.read(b),l.next(b));
               
            b= l.previous(b);

        }
         l.write(val,l.next(b));
     }
	
     cout<<l;
}


int ::eo_strings::freq_pari() {
	return pari.size();
}