#include "esame.hpp"
#include <iostream>
#include <string>
using namespace std;

class studente
{
public:
	studente();
	studente(string nome_esame , string nome_esame1,string nome_esame2,string nome_esame3,string nome_esame4);
	void setNome(string nome);
	string getNome()const ;
	void setMatricola (string matricola);
	string getMatricola() const; 

	int getNumeroEsami() const;
	int getEsame(string nome_esame) const;
	void setEsame(int voto, int posizione_esame) ;
	int getVotoEsame(int posizione_esame)const;
	double getMedia() const;
	void print(std::ostream& os)const;
	~studente();
friend std::ostream& operator<<(std::ostream& os, const studente&);

private:
	string matricola;
	string nome_studente;
	const int numero_esami=5;
	esame esami[5];
	int numero_esami_passati=0;
};