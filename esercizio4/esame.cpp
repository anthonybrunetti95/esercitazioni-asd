#include "esame.hpp"

esame::esame(){

}

esame::esame(string nome_esame) {
	this->nome=nome_esame;
}

esame::esame(string nome_esame, int voto){
	this->nome=nome_esame;
	this->voto_esame=voto;
	if(voto>18)
		this->sostenuto=1;
}

string esame::getEsame() const{
	return this->nome;
}

void esame::setEsame(string nome_esame){
		this->nome=nome_esame;	
}

int esame::getVoto() const {
	return this->voto_esame;

}

void esame::setVoto(int voto){
	this->voto_esame=voto;
	if(this->voto_esame>17)
		this->sostenuto=1;
	
}

bool esame::isSostenuto() const{
	return this->sostenuto;
}

void esame::print(std::ostream& os) const{
	
	os<<"		esame :"+ nome+ "\n";
	os<<"		voto:"+to_string(voto_esame)+ "\n";
	os<<"		passato:";
	if(sostenuto==1){

		os<<"si \n";
	}
	else
		os<<"no \n";

}