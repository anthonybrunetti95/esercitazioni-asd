#ifndef _ESAME_H
#define _ESAME_H

#include <iostream>
#include <string>
using namespace std;

class esame {
public:
	esame();
	esame(string);
	esame(string, int);
   	string getEsame() const;
   	void setEsame(string);
   	int getVoto() const;
   	void setVoto(int);
   	void print(ostream& os) const;
   	bool isSostenuto() const; 
private:
	string nome;
	bool sostenuto=0;
	int voto_esame=0;

};


#endif // _ESAME_H 

