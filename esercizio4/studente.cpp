#include "studente.hpp"
#include "esame.hpp"
studente::studente(){
	esame e("asd");
	esame e1("fisica");
	esame e2("calcolo");
	esame e3("mri");
	esame e4("icse");
	esami[0] = e;
	esami[1] = e1;
	esami[2] = e2;
	esami[3] = e3;
	esami[4] = e4;

}

studente::studente(string nome_esame , string nome_esame1,string nome_esame2,string nome_esame3,string nome_esame4){
	
	esame e(nome_esame);
	esame e1(nome_esame1);
	esame e2(nome_esame2);
	esame e3(nome_esame3);
	esame e4(nome_esame4);
	esami[0] = e;
	esami[1] = e1;
	esami[2] = e2;
	esami[3] = e3;
	esami[4] = e4;

}

void studente::setNome(string nome){
	this->nome_studente= nome;
}

string studente::getNome() const{
	return this->nome_studente;
}

void studente::setMatricola (string matricola){
	this->matricola=matricola;
}

string studente::getMatricola() const{
	return matricola;

}

int studente::getNumeroEsami() const{
	return numero_esami_passati;

}

int studente::getEsame(string nome_esame) const{
	for(int i=0;i<5;i++) 
		if(esami[i].getEsame() == nome_esame)
			return i;
	return -1;
}

void studente::setEsame(int voto, int posizione_esame){
	esami[posizione_esame].setVoto(voto);
		if(esami[posizione_esame].isSostenuto()==1)
			numero_esami_passati++;

}


int studente::getVotoEsame(int posizione_esame) const{
	if(esami[posizione_esame].isSostenuto()==1)
		return esami[posizione_esame].getVoto();
	return -1;
}


double studente::getMedia() const{
	int somma=0;
	for(int i=0;i<5;i++) 
		if(esami[i].isSostenuto()==1)
			somma=somma+esami[i].getVoto();
	return somma/numero_esami_passati;

}


void studente::print(std::ostream& os) const{
	os<<"	Nome: 	"+nome_studente+"\n";
	os<< "	Matricola: 	"+ matricola+"\n";
	os<<"	esami passati:	"+ to_string(numero_esami_passati)+"\n";
	os<<"	elnco esami:\n";
	for(int i=0; i<5; i++){
		esami[i].print(os);
		os<<"\n";
	}	
	os<<"	Media: "+to_string(this->getMedia())+"\n";

}

studente::~studente(){
	
}


std::ostream& operator<<(std::ostream& os, const studente& s){
    os << "\n ------ Studente -------- \n";
  	s.print(os);
    os << " ------------------------ \n";
    return os;
}
