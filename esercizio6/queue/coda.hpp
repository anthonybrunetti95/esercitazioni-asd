/**
 */
#pragma once
#include <iostream>

template <class T>
class Coda;

template <class T>
class QNode{
public:
    friend class Coda<T>;
    typename Coda<T>::value_type _valore;
    QNode<T>* _prev;
};

template <class T>
class Coda{
public:
    typedef T value_type;

    // Constructors and destructors
    Coda();
    Coda(const Coda<T>&);
    ~Coda();

    // Operators
    void crea_coda();
    bool is_empty() const;
    value_type leggi_coda() const;
    void pop();
    void push(const value_type&);
    size_t getlenght(){ return _lenght; }
    void operator=(const Coda<T>&);

    template <class Z>
    friend std::ostream& operator<<(std::ostream&, Coda<T>&);
    void print(std::ostream&) const;

private:
    QNode<T>* _testa;
    QNode<T>* _coda;
    int _lenght;

    void _copia(const Coda<T>&);
    void _clear();
};

template <class T>
Coda<T>::Coda(){
    crea_coda();
}


template <class T>
Coda<T>::Coda(const Coda<T>& coda){
    crea_coda();
    _copia(coda);
}

template <class T>
void Coda<T>::operator=(const Coda<T>& coda){
    _clear();
    _copia(coda);
}

template <class T>
void Coda<T>::_copia(const Coda<T>& coda){
    this->_testa = new QNode<T>;
    this->_testa->_valore = coda._testa->_valore;
    this->_lenght = coda._lenght;

    QNode<T>* prev_node = _testa;
    QNode<T>* cp_node = coda._testa->_prev;

    while(cp_node != nullptr){
        QNode<T>* new_node = new QNode<T>;

        new_node->_valore = cp_node->_valore;
        new_node->_prev = nullptr;
        cp_node = cp_node->_prev;
        

        prev_node->_prev = new_node;
        prev_node = new_node;

        if(cp_node == nullptr){
            _coda = new_node;
        }
    }
}

template <class T>
void Coda<T>::_clear(){
    while(!is_empty()){
        pop();
    }  
}

template <class T>
Coda<T>::~Coda(){
    _clear();
}

template <class T>
void Coda<T>::crea_coda(){
    _testa = nullptr;
    _coda = nullptr;
    _lenght = 0;
}

template <class T>
bool Coda<T>::is_empty() const{
    return (_testa == nullptr);
}

template <class T>
typename Coda<T>::value_type Coda<T>::leggi_coda() const{
    return _testa->_valore;
}

template <class T>
void Coda<T>::pop(){
    if(!is_empty()){
        QNode<T>* old_testa = _testa;
        _testa = _testa->_prev;
        delete old_testa;
    }

    --_lenght;
}

template <class T>
void Coda<T>::push(const value_type &value){
    QNode<T>* new_coda = new QNode<T>;
    new_coda->_valore = value;
    new_coda->_prev = nullptr;

    if(_lenght == 0){
        _testa = _coda = new_coda;
    }else{
        _coda->_prev = new_coda;
        _coda = new_coda;
    }

    ++_lenght;
}


template <class T>
void Coda<T>::print(std::ostream& os) const{
    QNode<T>* test = _testa;
    while(test != nullptr){
        os << test->_valore;
        test = test->_prev;
        if(test != nullptr) os << ",";
    }
}

template <class T>
std::ostream& operator<<(std::ostream& os, Coda<T>& coda){
    os << "[";
    coda.print(os);
    os << "]";
    return os;
}

