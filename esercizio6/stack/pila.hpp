#pragma once

#include <iostream>

template <class T>
class Pila;

template <class T>
class PNode{
public:
    friend class Pila<T>;
private:
    typename Pila<T>::value_type valore;
    PNode<T>* _prec;
};

template <class T>
class Pila{
public:
    typedef T value_type;

    // Constructors and destructors
    Pila();
    Pila(const Pila<T>&);
    ~Pila();

    // Operators
    void crea_Pila();
    bool is_empty() const;
    value_type leggi_Pila() const;
    void pop();
    void push(value_type);
    size_t getlenght(){ return lenght; }

    template <class Z>
    friend std::ostream& operator<<(std::ostream&, Pila<T>&);
    void print(std::ostream&) const;
    void operator=(const Pila<T>&);


private:
    PNode<T>* _testa;
    int lenght;

    void _copia(const Pila<T>&);
    void _clear();
};

template <class T>
Pila<T>::Pila(){
    crea_Pila();
}

template <class T>
Pila<T>::Pila(const Pila<T>& stk){
    crea_Pila();
    _copia(stk);
}

template <class T>
void Pila<T>::_copia(const Pila<T>& stk){
    this->_testa = new PNode<T>;
    this->_testa->valore = stk._testa->valore;
    this->lenght = stk.lenght;
    
    PNode<T>* prev_node = _testa;

    PNode<T>* cp_node = stk._testa->_prec;

    while(cp_node != nullptr){
        PNode<T>* nuovo_nodo = new PNode<T>;

        nuovo_nodo->valore = cp_node->valore;
        nuovo_nodo->_prec = nullptr;
        cp_node = cp_node->_prec;
        

        prev_node->_prec = nuovo_nodo;
        prev_node = nuovo_nodo;
    }

}

template <class T>
void Pila<T>::operator=(const Pila<T>& stk){
    _clear();
    _copia(stk);
}

template <class T>
void Pila<T>::_clear(){
    while(!is_empty()){
        pop();
    }
}

template <class T>
Pila<T>::~Pila(){
    _clear();
}

template <class T>
void Pila<T>::crea_Pila(){
    _testa = nullptr;
}

template <class T>
bool Pila<T>::is_empty() const{
    return (_testa == nullptr);
}

template <class T>
typename Pila<T>::value_type Pila<T>::leggi_Pila() const{
    return _testa->valore;
}

template <class T>
void Pila<T>::pop(){
    if(!is_empty()){
        PNode<T>* old_testa = _testa;
        _testa = _testa->_prec;
        delete old_testa;
    }
}

template <class T>
void Pila<T>::push(value_type value){
    PNode<T>* index=nullptr;    
    if(is_empty()==false){

    std::cout<<"ok1 ";
        for(int i=0; i<lenght;i++)
        {
                index = _testa;
                if(index->valore!=value)
                {
                    
                    PNode<T>* new_testa = new PNode<T>;
                    new_testa->valore = value;
                    new_testa->_prec = _testa;
                    _testa = new_testa;
                    ++lenght;
                    break ;
                }
            index = index->_prec;
        }
    }
    else 
    {
        PNode<T>* new_testa = new PNode<T>;
        new_testa->valore = value;
        new_testa->_prec = _testa;
        _testa = new_testa;
        ++lenght;
        
    }    
    
}

template <class T>
std::ostream& operator<<(std::ostream& os, Pila<T>& stk){
    os << "[";
    stk.print(os);
    os << "]";
    return os;
}

template <class T>
void Pila<T>::print(std::ostream& os) const{
    PNode<T>* temp = _testa;
    while(temp != nullptr){
        os << temp->valore;
        if(temp->_prec != nullptr){
            os <<",";
        }
        temp = temp->_prec;
    }
}