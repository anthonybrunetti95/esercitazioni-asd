#include "./queue/coda.hpp"
#include <vector>
#include <iostream>

using namespace std;

template <class T>
class MultipleStack;

template <class T>
class MultipleStack
{
    typedef typename Coda<T>::value_type value_type;
    
public:
    MultipleStack (unsigned int);
    void Push (const value_type &, unsigned int);
    value_type Pop (unsigned int) const;
    void Print();
private:
	unsigned int num;
	vector<Coda<T>*> C;
	
};

template <class T>
MultipleStack<T>:: MultipleStack(unsigned int n) {
	this->num=n;
	for(int i=0; i<num ;i++)
		C.push_back(new Coda<T>);


	
}

template <class T>
void MultipleStack<T>::Push(const value_type &valore, unsigned int n) {
	

	C[n]->push(valore);
	
}

template <class T>
typename MultipleStack<T>::value_type MultipleStack<T>::Pop(unsigned int n) const {

	return C[n]->leggi_coda();

}

template <class T>
void MultipleStack<T>::Print() {
		for(int i=0;i<num;i++){
			cout<<*C[i];
		}
}