#include <iostream>


/**
 */
#pragma once
#include <iostream>

template <class T>
class DCoda;

template <class T>
class QNode{
public:
    friend class DCoda<T>;
    typename DCoda<T>::value_type _valore;
    QNode<T>* _prev;
};

template <class T>
class DCoda{
public:
    typedef T value_type;

    // Constructors and destructors
    DCoda();
    DCoda(const DCoda<T>&);
    ~DCoda();

    // Operators
    void crea_DCoda();
    bool is_empty() const;
    value_type leggi_testa() const;
    value_type leggi_coda() const;
    void pop_testa();
    void pop_coda();
    void ins_coda(const value_type&);
    void ins_testa(const value_type &);
    size_t getlenght(){ return _lenght; }
    void operator=(const DCoda<T>&);

    template <class Z>
    friend std::ostream& operator<<(std::ostream&, DCoda<T>&);
    void print(std::ostream&) const;

private:
    QNode<T>* _testa;
    QNode<T>* _DCoda;

    int _lenght;

    void _copia(const DCoda<T>&);
    void _clear();
};

template <class T>
DCoda<T>::DCoda(){
    crea_DCoda();
}


template <class T>
DCoda<T>::DCoda(const DCoda<T>& DCoda){
    crea_DCoda();
    _copia(DCoda);
}

template <class T>
void DCoda<T>::operator=(const DCoda<T>& DCoda){
    _clear();
    _copia(DCoda);
}

template <class T>
void DCoda<T>::_copia(const DCoda<T>& DCoda){
    this->_testa = new QNode<T>;
    this->_testa->_valore = DCoda._testa->_valore;
    this->_lenght = DCoda._lenght;

    QNode<T>* prev_node = _testa;
    QNode<T>* cp_node = DCoda._testa->_prev;

    while(cp_node != nullptr){
        QNode<T>* new_node = new QNode<T>;

        new_node->_valore = cp_node->_valore;
        new_node->_prev = nullptr;
        cp_node = cp_node->_prev;
        

        prev_node->_prev = new_node;
        prev_node = new_node;

        if(cp_node == nullptr){
            _DCoda = new_node;
        }
    }
}



template <class T>
void DCoda<T>::_clear(){
    while(!is_empty()){
        pop_testa();
    }  
}

template <class T>
DCoda<T>::~DCoda(){
    _clear();
}

template <class T>
void DCoda<T>::crea_DCoda(){
    _testa = nullptr;
    _DCoda = nullptr;
    _lenght = 0;
}

template <class T>
bool DCoda<T>::is_empty() const{
    return (_testa == nullptr);
}

template <class T>
typename DCoda<T>::value_type DCoda<T>::leggi_testa() const{
    return _testa->_valore;
}

template <class T>
typename DCoda<T>::value_type DCoda<T>::leggi_coda() const{
    return _DCoda->_valore;
}

template <class T>
void DCoda<T>::pop_testa(){
    if(!is_empty()){
        QNode<T>* old_testa = _testa;
        _testa = _testa->_prev;
        delete old_testa;
    }

    --_lenght;
}

template <class T>
void DCoda<T>::pop_coda(){
    if(!is_empty()){
        QNode<T>* old_coda = _DCoda;
        QNode<T>* index= _testa;
        for(int i=0; i<_lenght-2;i++){
        	index= index->_prev;
        }

        _DCoda = index;
        delete old_coda;
        
    }

    --_lenght;
}



template <class T>
void DCoda<T>::ins_coda(const value_type &value){
    QNode<T>* new_DCoda = new QNode<T>;
    new_DCoda->_valore = value;
    new_DCoda->_prev = nullptr;

    if(_lenght == 0){
        _testa = _DCoda = new_DCoda;
    }else{

        _DCoda->_prev = new_DCoda;
        _DCoda = new_DCoda;
    }

    ++_lenght;
}

template <class T>
void DCoda<T>::ins_testa(const value_type &value){
    QNode<T>* new_testa = new QNode<T>;
    new_testa->_valore = value;
    new_testa->_prev = _testa;
	_testa = new_testa;


    ++_lenght;
}


template <class T>
void DCoda<T>::print(std::ostream& os) const{
    QNode<T>* test = _testa;
    while(test != nullptr){
        os << test->_valore;
        test = test->_prev;
        if(test != nullptr) os << ",";
    }
}

template <class T>
std::ostream& operator<<(std::ostream& os, DCoda<T>& DCoda){
    os << "[";
    DCoda.print(os);
    os << "]";
    return os;
}

