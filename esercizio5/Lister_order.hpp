#include "./list/linked_list.hpp"
#include <string>
using namespace std;

template <class T>
class Ordered_list{
	
public:
	typedef typename Linked_list<T>::value_type value_type;
	typedef typename Linked_list<T>::position position;
	Ordered_list();
	void insert(const value_type &); // inserisce un elemento
	value_type read(position) const;
	void remove(const value_type &); // rimuove un elemento
	bool search(const value_type &) const; // cerca un elemento
	void merge(const Ordered_list<T> &);  // fonde con una lista ordinata
	template <class Z>
	friend std::ostream& operator<<(std::ostream& , const Ordered_list<Z> &);
private:
  Linked_list<T> L;
};

template <class T>
 Ordered_list<T>::Ordered_list() {
	L.create();
}

template <class T>
void Ordered_list<T>::insert(const value_type &value) {

	if(L.empty()==1){
		L.push_front(value);
	
	}
	else if(search(value)==0) {
		L.push_back(value);
		L.insertion_sort();
	}
			
    
}

template <class T>
typename Ordered_list<T>::value_type Ordered_list<T>::read(position p) const {
	return L.read(p);
}

template <class T>
void Ordered_list<T>::remove(const value_type &value) {
	L.erase(L.find(value));
}

template <class T>
bool Ordered_list<T>::search(const value_type &value)const {
    for(position p = L.begin(); !(L.end(p)); p = L.next(p)){
        if(L.read(p) == value){
            return 1;
        }
    }
    return 0;
}

template<class T>
void Ordered_list<T>::merge(const Ordered_list &L1) {
	for(position p = L1.L.begin(); !(L1.L.end(p)); p = L1.L.next(p))
			insert(L1.read(p));


}

template <class T>
std::ostream& operator<<(std::ostream& os, const Ordered_list<T>& lista){
    os << "[";
    for(typename Linked_list<T>::position p = lista.L.begin(); !(lista.L.end(p)); p = lista.L.next(p)){
        os << lista.L.read(p);
        if(lista.L.next(p) != lista.L.previous(lista.L.begin()))
            os << ",";
    }
    os << "]\n";
    return os;
}
