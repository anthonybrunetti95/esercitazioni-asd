#include <iostream>
using namespace std;

class matrice {
  public:
  	typedef double tipoelem;
    matrice(int, int); /* costruttore */
    tipoelem leggimatrice(int, int) const;
    void scrivimatrice(int, int,const tipoelem);
    void prodottoScalare(double);
    matrice trasposta();
    void print(std::ostream& os) const;
	friend std::ostream& operator<<(std::ostream& os, const matrice&);
	matrice& operator=(const matrice&);
  private:
    int righe;
    int colonne;
    tipoelem **elementi;
};