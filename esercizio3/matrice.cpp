#include "matrice.hpp"

// costruttore

matrice::matrice(int c, int r){
  this->colonne = c;
  this->righe = r;
  // allocazione dinamica della matrice
  
  elementi = new tipoelem* [righe];
  for (int i=0; i < righe; i++){

    	elementi[i] = new tipoelem[colonne];
  		// inizializzazione degli elementi
		for(int j=0; j < colonne; j++)
			elementi[i][j]=0;
  	}
}

matrice::tipoelem matrice::leggimatrice(int r, int c) const{
	return elementi[r][c];
}


void matrice::scrivimatrice(int r, int c,const tipoelem elem) {
	elementi[r][c]=elem;
}

void matrice::print(std::ostream& os) const {

	for(int i=0; i<righe; i++) {
		for(int j=0; j<colonne; j++) {
			os<< to_string(leggimatrice(i,j));
			if(j!=colonne-1)
					os<<" , ";
		}
		if(i!=righe-1)
				os<< "\n";
 	}
	
}

std::ostream& operator<<(std::ostream& os, const matrice& Matrice){
    os << "\n matrice \n";
  	Matrice.print(os);
    
    return os;
}


void matrice::prodottoScalare(double k) {
	for(int i=0; i< this->righe; i++)
		for(int j=0; j< this->colonne; j++)
			scrivimatrice(i,j,leggimatrice(i,j)*k);	

}

matrice matrice::trasposta() {
	matrice m1(colonne,righe);
	for(int i=0; i< m1.colonne; i++)
		for(int j=0; j< m1.righe; j++) 
			m1.scrivimatrice(j,i,this->leggimatrice(i,j));	

		return m1;
}

matrice& matrice::operator=(const matrice& Matrice){
	matrice(Matrice.righe,Matrice.colonne);
	for(int i=0; i< Matrice.righe; i++)
		for(int j=0; j< Matrice.colonne; j++) 
			this->scrivimatrice(j,i,Matrice.leggimatrice(i,j));
	return *this;
}
