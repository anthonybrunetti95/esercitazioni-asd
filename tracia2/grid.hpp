#include "../esercizio6/list/linked_list.hpp"

#include <iostream>
using namespace std;

class cell{
int x;
int y;
public:
	cell();
	cell(int x, int y);
	cell(cell &c);
	int get_x();
	int get_y();
	void set_x(int x);
	void set_y(int y);
	void print(ostream&)const;
	friend std::ostream& operator<<(std::ostream&,  cell&);
	bool operator==(const cell &c);
	cell& operator=(const cell& c);

};

cell::cell() {

}
cell::cell(int x, int y) {
	this->x=x;
	this->y=y;
}

cell::cell(cell &c) {
	this->x=c.x;
	this->y=c.y;
}
int cell::get_x() {
	return x;
}
int cell::get_y() {
	return y;
}
void cell::set_x(int x) {
	this->x=x;
}
void cell::set_y(int y) {
	this->y=y;
}

void cell::print(std::ostream &os) const {
	os<<this->x;
	os<<",";
	os<<this->y;	
}

std::ostream& operator<<(std::ostream& os,  cell &c){
	os<<"{";
	c.print(os);
	os<<"}";
	return os;
}

cell& cell::operator=(const cell& c) {
	this->x=c.x;
	this->y=c.y;	
	return *this;
}

bool cell::operator==(const cell &c) {
	if(this->x==c.x && this->y==c.y)
		return true;
	return false;
}


class grid{
	
public:
	typedef typename  Linked_list<cell>::position position ;


//ricerca
position find(int x , int y) ;
// inserisce una cella viva nella griglia
void insert(const cell &);
// rimuove una cella nella griglia
void remove(const cell &);
// sposta a sinistra di una posizione la cella viva presente in posizione (x,y)
void moveLeft(int x, int y);
// sposta a destra di una posizione la cella viva presente in posizione (x,y)
void moveRight(int x, int y);
// sposta in alto di una posizione la cella viva presente in posizione (x,y)
void moveUpper(int x, int y);
// sposta in basso di una posizione la cella viva presente in posizione (x,y)
void moveDown(int x, int y);
// stabilisce se la cella viva presente in posizione (x,y) `e circondata,
// ovvero tutte le celle vicine alla cella data sono vive
bool surrounded(int x, int y);
// rimuove dalla griglia tutte le celle circondate
void removeSurrounded();
// ... //
void print();
private:
Linked_list<cell> cells;
//
};



void grid::insert(const cell &c) {
	cells.push_back(c);
}

void grid::remove(const cell &c) {
	if(cells.find(c)!=nullptr) {
		cells.erase(cells.find(c));
	}
}

 Linked_list<cell>::position grid::find(int x, int y)  {
 	Linked_list<cell>::position p; 
	for(  p=cells.begin(); !cells.end(p); p=cells.next(p)) {
		if(cells.read(p)==cell(x,y)) {
			
			return p;
		}
	}

	return nullptr;
}

void grid::moveLeft(int x, int y) {
	cell c;
	
	c=cells.read(find(x,y));
	c.set_x(c.get_x()+1);
	cells.write(c, find(x,y));

}


void grid::moveRight(int x , int y) {
	cell c;
	c=cells.read(find(x,y));
	c.set_x(c.get_x()-1);
	cells.write(c, find(x,y));

}

void grid::moveUpper(int x ,int y) {
	cell c;
	
	c=cells.read(find(x,y));
	c.set_y(c.get_y()-1);
	cells.write(c, find(x,y));

}

void grid::moveDown(int x, int y) {
	cell c;
	c=cells.read(find(x,y));
	c.set_y(c.get_y()+1);
	cells.write(c, find(x,y));
}

bool grid::surrounded(int x, int y) {
	return (find(x+1,y)!=nullptr && find(x-1,y)!=nullptr && find(x,y-1)!=nullptr && find(x,y+1)!=nullptr);
	

}


void grid::print() {
	cell c;
	for(Linked_list<cell>::position p=cells.begin(); !cells.end(p); p=cells.next(p)) {
		c= cells.read(p);
		cout<<c;
	}
}

void grid::removeSurrounded() {
	for(Linked_list<cell>::position p=cells.begin(); !cells.end(p); p=cells.next(p)) {
		if(surrounded(cells.read(p).get_x(),cells.read(p).get_y())==true) {
			cells.erase(p);
			return ;
		}
	}
}