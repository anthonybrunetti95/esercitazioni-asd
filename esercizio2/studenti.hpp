#ifndef _STUDENTE
#define _STUDENTE
#include <iostream>
#include <string>

using namespace std;

class  studente
{
private:
	int matricola;
	string nome;
	string cognome;
	int eta;
public:
	 studente();
	~ studente();
	/* funzioni per l'inserimento dei valori degli elementi della struttura */
	int setNome(const string nome);
	int setCognome(const string cognome);
	int setMatricola(const int matr);
	int setEta(const int eta);
	/* funzioni per ottenere i valori degli elementi della struttura */
	string getNome() const;
	string getCognome() const;
	int getMatricola() const;
	int getEta() const;
	void print(std::ostream& os) const;
	friend std::ostream& operator<<(std::ostream& os, const studente& Studente);

};

#endif /* _STUDENTE */

