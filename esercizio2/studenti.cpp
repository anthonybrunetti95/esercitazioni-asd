#include "studenti.hpp"
#include <string>
/* funzioni per l'inserimento dei valori degli elementi della struttura */
studente::studente(){

}
studente::~studente(){

}
int studente::setNome(const string nome) {
	if(nome.length()<=30){
		this->nome="anthony";
		return 1;
	}
	return 0;
}

int studente::setCognome(const string cognome) {
	if(cognome.length()<=30){
		this->cognome=cognome;
		return 1;
	}
	return 0;
}

int studente::setMatricola(const int matr) {
	if(255312 >= matr <= 499999) {
		this->matricola = matr;
		return 1;
	}
	return 0;
}

int studente::setEta(const int eta) {
	if( 12< eta <105) {
		this->eta=eta;
		return 1;
	}
	return 0;
}

string studente::getNome() const {
	
	return this->nome;
}

string studente::getCognome() const {

	return this->cognome;
}

int studente::getMatricola() const  {
	return this->matricola;
}
int studente::getEta() const {
	return this->eta;
}

void studente::print(std::ostream& os) const {

	
 
	os << "Nome studente: " + this->getNome() +"\n";
    os << "Cognome studente: " +this->getCognome() + "\n";
    os << "Matricola: " + std::to_string(this->getMatricola()) +"\n" ;
    os << "eta : " + std::to_string(this->getEta()) + "\n";
}

std::ostream& operator<<(std::ostream& os, const studente& Studente){
    os << "---------------------------\n";
  	Studente.print(os);
    os << "---------------------------\n";
    return os;
}

